package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config : The server configuration
type Config struct {
	Port string
}

// LoadConfig loading config.yml into a Config struct
func LoadConfig(fileName string) (*Config, error) {
	source, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = yaml.Unmarshal(source, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
