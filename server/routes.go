package server

func (s *Server) routes() {
	s.Router.HandleFunc("/", handleIndex)
	s.Router.HandleFunc("/health", handleHealth)
}
