package main

import (
	"log"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/srae/httpsrv-go/config"
	"gitlab.com/srae/httpsrv-go/server"
)

func main() {
	log.Println("started")
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	config, err := config.LoadConfig("config/config.yml")
	if err != nil {
		return errors.Wrap(err, "Load config.yml")
	}

	xsdServer := server.NewServer(config)

	log.Println("Running server on port ", xsdServer.GetPort())
	log.Fatal(http.ListenAndServe(":"+xsdServer.GetPort(), xsdServer))

	return nil
}
